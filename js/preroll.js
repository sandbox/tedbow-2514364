/**
 * @file preroll.js.
 */

(function ($) {
  /**
   *
   */
  Drupal.behaviors.wistia_preroll_formatter = {
    attach: function(context, settings) {
      var prerolls = Drupal.settings.wistia_preroll_formatter.preroll_videos;

      var total_weight = 0;
      var random_preroll = false;
      $.each(prerolls, function(key, value) {
        console.log(key, value);
        var weight = parseInt(value.video_weight);
        total_weight = total_weight + weight;
        prerolls[key].top_range = total_weight;
      });
      var random = Math.floor((Math.random() * total_weight) + 1);
      $.each(prerolls, function(key, value) {
        if (random <= prerolls[key].top_range) {
          random_preroll = prerolls[key];
          return false;
        }
      });
      // Now we should have callback
      var videos = settings.wistia_preroll_formatter.videos;
      $.each(videos, function(div_id, video_code) {
        preRollEmbed = Wistia.embed(random_preroll.video_code, {
          version: "v1",
          videoFoam: true,
          chromeless: true,
          container: div_id,
          stillUrl: video_code.still_image_url
        });
        preRollEmbed.bind("end", function() {
          preRollEmbed.remove();
          wistiaEmbed = Wistia.embed(video_code, {
            version: "v1",
            videoFoam: true,
            controlsVisibleOnLoad: true,
            container: div_id,
            autoPlay: true
          });
        });
      });
    }
  };

})(jQuery);
